# Chimpy

Chimpy is our good primate friend who helps the audience development team at Business Report generate CSVs of metrics related to our various email newsletters. Mailchimp segments out these metrics based on every email you send, so Chimpy fetches all of the sends for a given list and outputs a nice CSV that the team can then take and use as they like.