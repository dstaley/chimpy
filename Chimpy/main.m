//
//  main.m
//  Chimpy
//
//  Created by Dylan Staley on 8/6/13.
//  Copyright (c) 2013 Business Report. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <MacRuby/MacRuby.h>

int main(int argc, char *argv[])
{
    return macruby_main("rb_main.rb", argc, argv);
}
