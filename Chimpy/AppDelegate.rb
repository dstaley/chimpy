#
#  AppDelegate.rb
#  Chimpy
#
#  Created by Dylan Staley on 8/6/13.
#  Copyright 2013 Business Report. All rights reserved.
#

require 'gibbon'
require 'csv'

class AppDelegate
    attr_accessor :window
    attr_accessor :folderList
    attr_accessor :startDate
    attr_accessor :endDate
    
    def applicationDidFinishLaunching(a_notification)
        $api_key = "trololol87b81470a2b8a0-us2"
        loadFolders
    end
    
    def generateCSV(sender)
        gb = Gibbon::API.new($api_key)
        campaigns = gb.campaigns.list({filters: {folder_id: $folders[@folderList.indexOfSelectedItem]['folder_id'], sendtime_start: @startDate.dateValue.to_s[0..10] + "00:00:00", sendtime_end: @endDate.dateValue.to_s[0..10] + "00:00:00"}})
        csv_string = CSV.generate do |csv|
          csv << ["Subject", "Unique Opens", "Open Rate", "Emails Sent", "Hard Bounces", "Soft Bounces", "Unsubscribes", "Unique Clicks", "Click Rate"]
          campaigns['data'].each do |campaign|
            begin
                summary = gb.reports.summary({:cid => campaign['id']})
                csv << ["#{campaign['subject']}",
                        "#{summary['unique_opens']}",
                        "#{(summary['unique_opens'].to_f / summary['emails_sent']) * 100}",
                        "#{summary['emails_sent']}",
                        "#{summary['hard_bounces']}",
                        "#{summary['soft_bounces']}",
                        "#{summary['unsubscribes']}",
                        "#{summary['unique_clicks']}",
                        "#{(summary['unique_clicks'].to_f / summary['emails_sent']) * 100}"]
            rescue Gibbon::MailChimpError
                puts "No campaign stats"
            end
          end
        end
        
        panel = NSSavePanel.savePanel
        panel.setCanChooseDirectories(false)
        panel.setNameFieldStringValue("export.csv")
        result = panel.runModal()
        if(result == NSOKButton)
            path = panel.filename
            @file_path = path
            path = File.expand_path(@file_path)
            File.open(path, 'w') { |file| file.write(csv_string) }
        end
        playSound("Glass")
    end
    
    def loadFolders
        gb = Gibbon::API.new($api_key)
        $folders = gb.folders.list({:type => 'campaign'})
        $folders.each {|x| @folderList.addItemWithTitle(x['name'])}
    end

    def playSound(name)
        sound = NSSound.soundNamed(name)
        sound.play
    end
end

